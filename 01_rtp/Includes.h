#pragma once

#include <iostream>
#include <fstream>

#include <cmath>
#include <ctime>

#include <chrono>
#include <thread>
#include <algorithm>
#include <random>
#include <numeric>
#include <numbers>
#include <execution>

#include <string>
#include <sstream>

#include <array>
#include <vector>
#include <map>
#include <queue>
#include <set>
#include <span>

#include <type_traits>
#include <emmintrin.h>


