#pragma once

#include "Includes.h"



template <class DataType>
class VectorDynamic {

public:

	int size = 0;
	std::vector<DataType> data;

	// Constructors

	VectorDynamic() {

	}
	explicit VectorDynamic(int size, DataType val = 0) {

		this->size = size;
		this->data.resize(size);
		for (int i = 0; i < size; i++) {
			this->data[i] = val;
		}
	}
	VectorDynamic(std::initializer_list<DataType> list) {

		this->size = list.size();
		this->data = list;
	}

	// Convert from any type

	template <class DataType2>
	VectorDynamic(const VectorDynamic<DataType2>& vec) {

		this->data.resize(vec.size);
		for (int i = 0; i < vec.size; i++) {
			this->data[i] = (DataType)vec.data[i];
		}
	}

	// Access operator

	DataType& operator[](int index) {

		return this->data[index];
	}

	// Addition

	VectorDynamic operator+(const VectorDynamic& vec) const {

		if (this->size != vec.size) {
			throw;
		}

		VectorDynamic result(this->size);
		for (int i = 0; i < this->size; i++) {
			result.data[i] = this->data[i] + vec.data[i];
		}

		return result;
	}
	VectorDynamic operator+(DataType val) const {

		VectorDynamic result(this->size);
		for (int i = 0; i < this->size; i++) {
			result.data[i] = this->data[i] + val;
		}

		return result;
	}
	VectorDynamic& operator+=(const VectorDynamic& vec) {

		if (this->size != vec.size) {
			throw;
		}

		for (int i = 0; i < this->size; i++) {
			this->data[i] += vec.data[i];
		}

		return *this;
	}
	VectorDynamic& operator+=(DataType val) {

		for (int i = 0; i < this->size; i++) {
			this->data[i] += val;
		}

		return *this;
	}

	// Subtraction

	VectorDynamic operator-(const VectorDynamic& vec) const {

		if (this->size != vec.size) {
			throw;
		}

		VectorDynamic result(this->size);
		for (int i = 0; i < this->size; i++) {
			result.data[i] = this->data[i] - vec.data[i];
		}

		return result;
	}
	VectorDynamic operator-(DataType val) const {

		VectorDynamic result(this->size);
		for (int i = 0; i < this->size; i++) {
			result.data[i] = this->data[i] - val;
		}

		return result;
	}
	VectorDynamic& operator-=(const VectorDynamic& vec) {

		if (this->size != vec.size) {
			throw;
		}

		for (int i = 0; i < this->size; i++) {
			this->data[i] -= vec.data[i];
		}

		return *this;
	}
	VectorDynamic& operator-=(DataType val) {

		for (int i = 0; i < this->size; i++) {
			this->data[i] -= val;
		}

		return *this;
	}

	// Multiplication

	VectorDynamic operator*(const VectorDynamic& vec) const {

		if (this->size != vec.size) {
			throw;
		}

		VectorDynamic result(this->size);
		for (int i = 0; i < this->size; i++) {
			result.data[i] = this->data[i] * vec.data[i];
		}

		return result;
	}
	VectorDynamic operator*(DataType val) const {

		VectorDynamic result(this->size);
		for (int i = 0; i < this->size; i++) {
			result.data[i] = this->data[i] * val;
		}

		return result;
	}
	VectorDynamic& operator*=(const VectorDynamic& vec) {

		if (this->size != vec.size) {
			throw;
		}

		for (int i = 0; i < this->size; i++) {
			this->data[i] *= vec.data[i];
		}

		return *this;
	}
	VectorDynamic& operator*=(DataType val) {

		for (int i = 0; i < this->size; i++) {
			this->data[i] *= val;
		}

		return *this;
	}

	// Division

	VectorDynamic operator/(const VectorDynamic& vec) const {

		if (this->size != vec.size) {
			throw;
		}

		VectorDynamic result(this->size);
		for (int i = 0; i < this->size; i++) {
			result.data[i] = this->data[i] / vec.data[i];
		}

		return result;
	}
	VectorDynamic operator/(DataType val) const {

		VectorDynamic result(this->size);
		for (int i = 0; i < this->size; i++) {
			result.data[i] = this->data[i] / val;
		}

		return result;
	}
	VectorDynamic& operator/=(const VectorDynamic& vec) {

		if (this->size != vec.size) {
			throw;
		}

		for (int i = 0; i < this->size; i++) {
			this->data[i] /= vec.data[i];
		}

		return *this;
	}
	VectorDynamic& operator/=(DataType val) {

		for (int i = 0; i < this->size; i++) {
			this->data[i] /= val;
		}

		return *this;
	}

	// Assignment, from any type

	template <class DataType2>
	VectorDynamic<DataType>& operator=(const VectorDynamic<DataType2>& vec) {

		this->size = vec.size;
		this->data.resize(vec.size);
		for (int i = 0; i < vec.size; i++) {
			this->data[i] = (DataType)vec.data[i];
		}

		return *this;
	}

	// Comparison

	bool operator==(const VectorDynamic& vec) const {

		if (this->size != vec.size) {
			throw;
		}

		int equals = 0;
		for (int i = 0; i < this->size; i++) {
			equals += this->data[i] == vec.data[i] ? 1 : 0;
		}

		return equals == this->size;
	}
	bool operator!=(const VectorDynamic& vec) const {

		return !(*this == vec);
	}

	// Negate

	VectorDynamic operator - () const {

		return *this * -1;
	}

	// Properties

	DataType magnitude() const {

		return (DataType)std::sqrt(this->magnitude2());
	}
	DataType magnitude2() const {

		DataType sum = 0;
		for (int i = 0; i < this->size; i++) {
			sum += this->data[i] * this->data[i];
		}

		return sum;
	}
};



namespace VectorDynamicMath {

	template <class DataType>
	VectorDynamic<DataType> Normalize(const VectorDynamic<DataType>& vec) {

		return vec / vec.magnitude();
	}

	template <class DataType>
	DataType Dot(const VectorDynamic<DataType>& vec1, const VectorDynamic<DataType>& vec2) {

		if (vec1.size != vec2.size) {
			throw;
		}

		DataType sum = 0;
		for (int i = 0; i < vec1.size; i++) {
			sum += vec1.data[i] * vec2.data[i];
		}

		return sum;
	}

	template <class DataType>
	VectorDynamic<DataType> Apply(const VectorDynamic<DataType>& vec, DataType(*function)(DataType value)) {

		VectorDynamic<DataType> result(vec.size);
		for (int i = 0; i < vec.size; i++) {
			result.data[i] = function(vec.data[i]);
		}

		return result;
	}

	template <class DataType>
	VectorDynamic<DataType> Apply(const VectorDynamic<DataType>& vec1, const VectorDynamic<DataType>& vec2, DataType(*function)(DataType value1, DataType value)) {

		if (vec1.size != vec2.size) {
			throw;
		}

		VectorDynamic<DataType> result(vec1.size);
		for (int i = 0; i < vec1.size; i++) {
			result.data[i] = function(vec1.data[i], vec2.data[i]);
		}

		return result;
	}
}

