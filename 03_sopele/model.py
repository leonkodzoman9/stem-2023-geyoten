import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

x_train = pd.read_csv("X_train.csv")
y_train = pd.read_csv("y_train.csv")
x_test_true = pd.read_csv("X_test.csv")

df_col = x_test_true[['Id']].copy()
print(df_col)
x_test_true = x_test_true.drop('Id', axis = 1)
x_train = x_train.drop('Id', axis = 1)
y_train = y_train.drop('Id', axis = 1)
x_train, x_test, y_train, y_test = train_test_split(x_train, y_train, test_size = 0.3)

from sklearn.ensemble import RandomForestClassifier
model = RandomForestClassifier().fit(x_train, y_train)

prediction = model.predict(x_test)

prediction = pd.DataFrame(prediction, columns=['labels'])

print('Accuracy: ', accuracy_score(prediction, y_test))

prediction = model.predict(x_test_true)
prediction = pd.DataFrame(prediction, columns=['labels'])

df_col['labels'] = prediction['labels']
print(df_col.head())

df_col = df_col.set_index('Id', drop=True)
df_col.to_csv("y_test.csv")