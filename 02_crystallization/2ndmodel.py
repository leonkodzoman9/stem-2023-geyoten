import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestRegressor

x_train = pd.read_csv("kaggle_x_train.csv")
y_train = pd.read_csv("kaggle_y_train.csv")
x_test_true = pd.read_csv("kaggle_x_test.csv")
##print(y_train.head(200))
df_col = x_test_true[['Id']].copy()

print(x_train.shape, y_train.shape)
x_train = x_train.drop('Id', axis=1)
y_train = y_train.drop('Id', axis = 1)
##x_train, x_test, y_train, y_test = train_test_split(x_train, y_train, test_size = 0.3)

model = RandomForestRegressor().fit(x_train, y_train)

# prediction = model.predict(x_test)

# prediction = pd.DataFrame(prediction, columns=['Conc'])

# print('Accuracy: ', mean_squared_error(prediction, y_test))

x_test_true = x_test_true.drop('Id', axis = 1)

prediction = model.predict(x_test_true)

prediction = pd.DataFrame(prediction, columns=['Conc'])
df_col['Conc'] = prediction['Conc']
df_col = df_col.set_index('Id', drop=True)
print(df_col.head())

df_col.to_csv("y_test.csv")