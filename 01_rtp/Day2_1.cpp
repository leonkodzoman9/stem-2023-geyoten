#include <iostream>

#include "NeuralNetwork.h"


static constexpr std::string_view PathX = "stem-games-2023-predicting-athlete-rtp/kaggle_x_train.csv";
static constexpr std::string_view PathXTest = "stem-games-2023-predicting-athlete-rtp/kaggle_x_test.csv";
static constexpr std::string_view PathY = "stem-games-2023-predicting-athlete-rtp/kaggle_y_train.csv";
static constexpr int Size = 3386;



int main() {

	// Neuralna mreza je vlastita implementacije jednostavnog multilayer perceptrona 
	// gdje mogu mijenjati broj slojeva, aktivacijske funkcije, inicijalizaciju weightova i learning rate

	srand(time(0));

	// Ucitati sve fileove

	std::ifstream fileX(PathX.data());
	std::ifstream fileXTest(PathXTest.data());
	std::ifstream fileY(PathY.data());

	// Izvuci podatke iz svih i napraviti datasetove
	// Podatci se skaliraju da budu izmedju 0 i 1

	NeuralNetworkDataset datasetTrain;
	NeuralNetworkDataset datasetValidation;
	NeuralNetworkDataset datasetTest;
	for (int i = 0; i < Size; i++) {
		int values[18];
		fileX >> values[0];
		for (int j = 0; j < 18; j++) {
			fileX >> values[j];
		}
		VectorDynamic<double> dataIn(18);
		dataIn[0] = values[0] / 50.0;
		dataIn[1] = values[1];
		dataIn[2] = values[2];
		dataIn[3] = values[3] / 3.0;
		dataIn[4] = values[4] / 3.0;
		dataIn[5] = values[5] / 3.0;
		dataIn[6] = values[6];
		dataIn[7] = values[7];
		dataIn[8] = values[8] / 4.0;
		dataIn[9] = values[9];
		dataIn[10] = values[10];
		dataIn[11] = values[11];
		dataIn[12] = values[12] / 3.0;
		dataIn[13] = values[13];
		dataIn[14] = values[14];
		dataIn[15] = values[15];
		dataIn[16] = values[16];
		dataIn[17] = values[17];

		VectorDynamic<double> dataOut(1);
		fileY >> values[0];
		fileY >> values[0];
		dataOut[0] = values[0] / 50.0;

		// Podijelim na skup za treniranje i validaciju

		if (rand() / (double)RAND_MAX < 0.7) {
			datasetTrain.inputs.push_back(dataIn);
			datasetTrain.outputs.push_back(dataOut);
		}
		else {
			datasetValidation.inputs.push_back(dataIn);
			datasetValidation.outputs.push_back(dataOut);
		}
	}

	std::vector<int> IDs(847);
	for (int i = 0; i < IDs.size(); i++) {
		int values[18];
		fileXTest >> IDs[i];
		for (int j = 0; j < 18; j++) {
			fileXTest >> values[j];
		}
		VectorDynamic<double> dataIn(18);
		dataIn[0] = values[0] / 50.0;
		dataIn[1] = values[1];
		dataIn[2] = values[2];
		dataIn[3] = values[3] / 3.0;
		dataIn[4] = values[4] / 3.0;
		dataIn[5] = values[5] / 3.0;
		dataIn[6] = values[6];
		dataIn[7] = values[7];
		dataIn[8] = values[8] / 4.0;
		dataIn[9] = values[9];
		dataIn[10] = values[10];
		dataIn[11] = values[11];
		dataIn[12] = values[12] / 3.0;
		dataIn[13] = values[13];
		dataIn[14] = values[14];
		dataIn[15] = values[15];
		dataIn[16] = values[16];
		dataIn[17] = values[17];

		datasetTest.inputs.push_back(dataIn);
	}

	fileY.close();
	fileX.close();
	fileXTest.close();

	std::ofstream out("out.csv");

	VectorDynamic<double> scale(1, 50);
	NeuralNetwork network({ 18,48,48,1 }, 0.1, WeightInitialization::HE, ActivationFunction::SIGMOID);

	// Treniranje na testnom skupu 300 iteracija

	int itersMax = 300;
	for (int iters = 0; iters < itersMax; iters++) {

		network.train(datasetTrain);
		printf("%3d/%3d, %f\n", iters + 1, itersMax, network.getAverageError(datasetTrain, scale));
	}

	// Izracunam predvidjanja za test example

	for (int i = 0; i < datasetTest.inputs.size(); i++) {
		network.predict(datasetTest.inputs[i]);
		out << IDs[i] << "," << int(network.layersOutput.back()[0] * 50) << "\n";
	}

	out.close();

	return 0;
}